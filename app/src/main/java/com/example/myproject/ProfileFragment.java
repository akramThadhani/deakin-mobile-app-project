package com.example.myproject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    TextView name, email, phone, address;
    String uid;


    private ProgressDialog mProgressDialog;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = null;
        mProgressDialog = ProgressDialog.show(getContext(),"Loading Profile", "Please wait...",false,false);

        //check for current user profile details
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid = user.getUid();
        Log.i("firebaseUID", uid);

        if(HomePage.profilePresent)
        {

            view =  inflater.inflate(R.layout.fragment_profile, container, false);

            DatabaseReference userList = FirebaseDatabase.getInstance().getReference().child("User List");
            Log.i("userRef1", String.valueOf(userList));

            DatabaseReference hello = userList.child(uid);
            Log.i("hello", hello.toString());

            hello.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Log.i("set text for full name", dataSnapshot.child("Full Name").getValue(String.class));
                    name.setText(dataSnapshot.child("Full Name").getValue(String.class));
                    email.setText(dataSnapshot.child("Email").getValue(String.class));
                    phone.setText(dataSnapshot.child("Phone").getValue(String.class));
                    address.setText(dataSnapshot.child("Address").getValue(String.class));
                    Log.i("All set", "Profile done");

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            name = view.findViewById(R.id.fullNamePutTextHere);
            email = view.findViewById(R.id.emailPutTextHere);
            phone = view.findViewById(R.id.phonePutTextHere);
            address = view.findViewById(R.id.addressPutTextHere);

            mProgressDialog.dismiss();

        }
        else
        {
            //this will be a first time user so we display to first complete an order to create a profile
            view =  inflater.inflate(R.layout.empty_profile, container, false);
            mProgressDialog.dismiss();

            Log.i("Profile empty", "no profile in DB");


        }

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i("onstart", "we are here");






    }

}
