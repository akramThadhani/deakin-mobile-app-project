package com.example.myproject;

public class Product {

    //this is the class of Products object type
    private int id, image;
    private String title;

    //class constructor
    public Product(int id, int image, String title)
    {
        this.id = id;
        this.image = image;
        this.title = title;
    }

    //create getter and setter methods for the ID
    public  int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    //create getter and setter methods for image view and text view
    public int getImage()
    {
        return image;
    }
    public void setImage(int image)
    {
        this.image = image;
    }
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
}
