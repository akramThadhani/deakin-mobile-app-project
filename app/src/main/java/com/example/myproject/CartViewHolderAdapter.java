package com.example.myproject;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CartViewHolderAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView productTitle, productPrice;


    private itemClickListener itemClickListener;

    public CartViewHolderAdapter(@NonNull View itemView) {
        super(itemView);

        productTitle = itemView.findViewById(R.id.titleTextView);
        productPrice = itemView.findViewById(R.id.priceTextView);

    }

    @Override
    public void onClick(View v) {
        itemClickListener.onItemClick(v, getAdapterPosition(),false);

    }

    public void setItemClickListener(itemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public interface itemClickListener {


        void onItemClick(View v, int adapterPosition, boolean b);
    }

}


