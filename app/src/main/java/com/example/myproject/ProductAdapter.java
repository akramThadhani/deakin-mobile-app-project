package com.example.myproject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.productViewHolder> {


    private List<Item> itemList = new ArrayList<>();
    private Context context;
    int pos;
    //initializing a global variable to be able to be accessed through the class
    public onItemListener mOnItemListener;

    //constructor for productAdapter
    //we pass the List of Item object types and the context and the onItemListner which will get the data when the view holder is clicked
    public ProductAdapter(List<Item> itemList, Context context, onItemListener onItemListener)
    {
        this.itemList = itemList;
        this.context = context;
        this.mOnItemListener = onItemListener;
    }
    //implementing default methods of recycler view adapter
    @NonNull
    @Override
    public productViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //method to return a viewHolder to bind (in the next method)
        //this method outputs the Views (image and text) initialized from the layout file (product_recyler_layout)
        View itemView = LayoutInflater.from(context).inflate(R.layout.product_recycler_layout, viewGroup, false);
        productViewHolder vHolder = new productViewHolder(itemView, mOnItemListener);
        return vHolder;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    //function to bind the data to the linear layout elements
    @Override
    public void onBindViewHolder(@NonNull final productViewHolder productViewHolder, final int i) {
        //we bind the view holder data all together in this method
        //we get the data through the get and set methods implemented in the Product class
        productViewHolder.productImageView.setImageResource(itemList.get(i).getImage());
        productViewHolder.productTextView.setText(itemList.get(i).getTitle());
        productViewHolder.productPrice.setText(itemList.get(i).getPrice());
        //onClicklistener to get the position of the view which the user clicked
        productViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("User Click: ", "card view Layout");
                //getting the position
                pos = productViewHolder.getAdapterPosition();
                mOnItemListener.onItemClick(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        //this method gets the size of the list
        return itemList.size();
    }

    //creating a nested class of the view holder to extend the class of RecyclerView.ViewHolder and implement onClickListener
    public static class productViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //initialize the views or elements
        ImageView productImageView;
        TextView productTextView;
        TextView productPrice;
        LinearLayout linearLayout;
        CardView cardView;

        onItemListener onItemListener;

        public productViewHolder(@NonNull View itemView, onItemListener onItemListener ) {
            super(itemView);
            //assign the elements to each Id for the reference
            productImageView = itemView.findViewById(R.id.productIconImageView);
            productTextView = itemView.findViewById(R.id.productTitleTextView);
            productPrice = itemView.findViewById(R.id.productPriceTextView);
            linearLayout = itemView.findViewById(R.id.LinearLayout);
            cardView = itemView.findViewById(R.id.cardView);

            this.onItemListener = onItemListener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }
    public interface onItemListener {
        void onItemClick(int position);
    }
}
