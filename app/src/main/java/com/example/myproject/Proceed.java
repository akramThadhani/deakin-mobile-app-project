package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Proceed extends AppCompatActivity {


    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    public TextView header,  fullName , phone, address, total;
    public EditText inputName, inputPhone, inputAddress;
    public Button confirmBtn;

    public static boolean orderArrived = false;
    public static boolean profileCreated = false;

    public static String confirmedName;
    public static String confirmedNumber;
    public static String confirmedAddress;

    public static int ORDERNUMBER;

    public String uid;

    public List<Item> itemList = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed);

        header = findViewById(R.id.headerTextView);
        fullName = findViewById(R.id.nameTextView);
        phone = findViewById(R.id.phoneTextView);
        address = findViewById(R.id.addressTextView);
        total = findViewById(R.id.totalTextView);

        final String cartValue = String.valueOf(ItemFragment.cartTotalValue);
        total.setText("Total: $ " + cartValue);

        inputName = findViewById(R.id.nameEditText);
        inputPhone = findViewById(R.id.phoneEditText);
        inputAddress = findViewById(R.id.addressEditText);

        Random random = new Random();
        ORDERNUMBER = random.nextInt(10000) ;

        //if guest has profile (means they have previously purchased) then load the details
        if(HomePage.profilePresent)
        {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            uid = user.getUid();
            Log.i("firebaseUID", uid);

            DatabaseReference userList = FirebaseDatabase.getInstance().getReference().child("User List");
            Log.i("userRef1", String.valueOf(userList));

            DatabaseReference hello = userList.child(uid);
            Log.i("hello", hello.toString());

            hello.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Log.i("set text for full name", dataSnapshot.child("Full Name").getValue(String.class));
                    inputName.setText(dataSnapshot.child("Full Name").getValue(String.class));
                    inputPhone.setText(dataSnapshot.child("Phone").getValue(String.class));
                    inputAddress.setText(dataSnapshot.child("Address").getValue(String.class));
                    Log.i("All set", "Profile done");

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String userId= user.getUid();

        confirmBtn = findViewById(R.id.confirmBtn);

        recyclerView = (RecyclerView) findViewById(R.id.proceedRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //creating a reference to the database so we can pull the data and build it onto the recycler view using a firebase recycler view adapter
        //this adapter takes in our custom adapter and binds the layout onto the proceed activity layout
        final DatabaseReference cartListRef = FirebaseDatabase.getInstance().getReference().child("Cart List");

        //Building the path to the database to extract the data from the tree
        FirebaseRecyclerOptions<Item> options = new FirebaseRecyclerOptions.Builder<Item>()
                .setQuery(cartListRef.child("User View").child(userId)
                .child("Items") , Item.class)
                .build();

        FirebaseRecyclerAdapter<Item, ConfirmationListAdapter> adapter = new
            FirebaseRecyclerAdapter<Item, ConfirmationListAdapter>(options) {
                @Override
                protected void onBindViewHolder(@NonNull ConfirmationListAdapter confirmationListAdapter, int i, @NonNull Item item) {

                    confirmationListAdapter.productTitle.setText(item.getTitle());
                    confirmationListAdapter.productPrice.setText(item.getPrice());

                    String title = item.getTitle();
                    String price = item.getPrice();

                    Log.i("OnBinD: ", title);
                    Log.i("OnBinD: ", price);

                    int image = item.getImage();

                    Item product = new Item(i, image, title, price);
                    itemList.add(product);

                }
                @NonNull
                @Override
                public ConfirmationListAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.confirmation_list_layout, parent, false);

                    return new ConfirmationListAdapter(view);
                }

            };

        recyclerView.setAdapter(adapter);
        adapter.startListening();

    }
    public void confirmFunction(@NonNull View view)
    {
        confirmBtn = view.findViewById(R.id.confirmBtn);

        //get the input details from the user for the delivery
        confirmedName = inputName.getText().toString();
        confirmedNumber = inputPhone.getText().toString();
        confirmedAddress = inputAddress.getText().toString();


        Log.i("Confirm Clicked:", "Confirm function");

        Log.i("Confirmed Name: ", confirmedName);

        Log.i("Confirmed Phone: ", confirmedNumber);

        Log.i("Confirmed Address: ", confirmedAddress);

        //print out the item title and price for each item added to the log

        for (int i = 0; i < itemList.size(); i++){

            int x = i + 1 ;

            String itemTitle = itemList.get(i).getTitle();
            String itemPrice = itemList.get(i).getPrice();

            Log.i("Item: "+ x, itemTitle + " " + itemPrice );

        }


        //send an email confirmation with main details

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String userId = user.getUid();
        String userEmail = user.getEmail();

        String subject = "Confirmation on your Order " + ORDERNUMBER;
        String message =
        "Your oder is now confirmed, Thank you for placing your order with Anything Online \n Order Number: " + ORDERNUMBER +
                "\n Address: " + confirmedAddress +
                "\n \n \n \n Have a nice day!";

        Log.i("userID: ", userId);
        Log.i("userEmail: ", userEmail);

        //send email confirmation
        JavaMailAPI javaMailAPI = new JavaMailAPI(this, userEmail, subject, message);
        javaMailAPI.execute();

        //take all the user info to the DB and use it to load up the profile page.
        //username, email, Full name, Phone, Address and the UID. so if the DB UID matches the UID saved. then load up all these details.

        Log.i("Saving user details: ", "DB");

        DatabaseReference userListRef = FirebaseDatabase.getInstance().getReference().child("User List");

        final HashMap<String, Object> cartMap = new HashMap<>();
        cartMap.put("Full Name", confirmedName);
        cartMap.put("Email", userEmail);
        cartMap.put("Phone", confirmedNumber);
        cartMap.put("Address", confirmedAddress);
        cartMap.put("UID", userId);

        userListRef.child(userId)
                .updateChildren(cartMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            Log.i("Task: ","Success");
                        }

                    }
                });


        //profile page should get verified after the first order is complete

        //create a new fragment for the processing delivery page

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new OrderDelivery();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.outerLayout, fragment).addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null)
        {
            Fragment fragment = new orderArrived();
            getSupportFragmentManager().beginTransaction().replace(R.id.outerLayout, fragment).addToBackStack(null).commit();

        }
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
            Log.i("onBackpressed: ", "going back");

        } else
        {
            getSupportFragmentManager().popBackStack();
        }

    }

}
