package com.example.myproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimatedStateListDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Layout;
import android.text.method.ArrowKeyMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.xmlpull.v1.XmlPullParser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingCart extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Button proceedBtn;
    public TextView totalAmount;

    List<Item> itemList;

    View cartViewHolderView;

    private boolean cartEmpty = ItemFragment.cartEmpty;

    public EditText username;

    public int cartItemNo;

    public String cIn ;

    public int cartItemPosition;

    public ShoppingCart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


       View view = null;

        if (cartEmpty == true)
        {
            view = inflater.inflate(R.layout.empty_cart, container, false);

//            username = (EditText)  view.findViewById(R.id.userNameEditText);
            proceedBtn = (Button) view.findViewById(R.id.proceedBtn);
            totalAmount = (TextView) view.findViewById(R.id.totalAmount);

        }

        if (cartEmpty == false)
        {
            Log.i("loading shopping cart", "Fragment ");
            view = inflater.inflate(R.layout.fragment_shopping_cart, container, false);

//            username = (EditText)  view.findViewById(R.id.userNameEditText);
            proceedBtn = (Button) view.findViewById(R.id.proceedBtn);
            totalAmount = (TextView) view.findViewById(R.id.totalAmount);

            recyclerView = (RecyclerView) view.findViewById(R.id.cartListRecycler);
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(this.getContext());
            recyclerView.setLayoutManager(layoutManager);

            String cartValue = String.valueOf(ItemFragment.cartTotalValue);

                totalAmount.setText("Total: $ " + cartValue);

                proceedBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.i("PROCEED CLICKED", "OK");

                        Intent intent = new Intent(getContext(), Proceed.class);

                        startActivity(intent);

                    }
                });

        }

        return view;
    }


    
    @Override
    public void onStart() {
        super.onStart();

        if (cartEmpty == false)
        {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final String userId= user.getUid();

            final DatabaseReference cartListRef = FirebaseDatabase.getInstance().getReference().child("Cart List");

                cartItemNo = ItemFragment.cartItemNumber;
                cIn = String.valueOf(cartItemNo);

                Log.i("CIN ", cIn);
                Log.i("cartItemNo: ", String.valueOf(cartItemNo));
                Log.i("cartItemNUMBER: ", String.valueOf(ItemFragment.cartItemNumber));

//            final DatabaseReference cartItemValue = cartListRef.child(String.valueOf(cartItemNo)).getRef();
                    //Building the path to the database to extract the data from the tree
            FirebaseRecyclerOptions<Item> options = new FirebaseRecyclerOptions.Builder<Item>()
                    .setQuery(cartListRef.child("User View").child(userId)
                    .child("Items") , Item.class)
                    .build();

            FirebaseRecyclerAdapter<Item, CartViewHolderAdapter> adapter = new
            FirebaseRecyclerAdapter<Item, CartViewHolderAdapter>(options) {
                @Override
                protected void onBindViewHolder(@NonNull final CartViewHolderAdapter cartViewHolderAdapter, int i, @NonNull final Item item) {

                    final String title = item.getTitle();
                    String price = item.getPrice();
                    Log.i("OnBinD: ", title);
                    Log.i("OnBinD: ", price);

                    cartViewHolderAdapter.productTitle.setText(item.getTitle());
                    cartViewHolderAdapter.productPrice.setText(item.getPrice());

                    cartViewHolderAdapter.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //gets the position of the View clicked
                            int itemPos = cartViewHolderAdapter.getLayoutPosition();
                            Log.i("itemPosHELLO: ", String.valueOf(itemPos));

                            final String itemId = cartViewHolderAdapter.productTitle.getText().toString();
                            Log.i("itemIDtitle: ", itemId);
                            cartItemPosition = itemPos ;

                            CharSequence options[] = new CharSequence[]
                                    {
                                            "Remove From Cart"
                                    };

                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Cart Options");

                            builder.setItems(options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (which == 0)
                                    {
                                        if (itemId == title)
                                        {
                                            cartListRef.child("User View").child(userId).child("Items")
                                            .child(itemId)
                                            .removeValue()
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful())
                                                    {
                                                        ItemFragment.cartItems--;
                                                        ItemFragment.cartItemNumber --;

                                                        String itemValue = cartViewHolderAdapter.productPrice.getText().toString();

                                                        Log.i("itemValue: ", itemValue);

                                                        ItemFragment.productValue = Integer.parseInt(itemValue.replaceAll("[\\D]",""));

                                                        Log.i("IValue: ", String.valueOf(ItemFragment.productValue));


                                                        ItemFragment.cartTotalValue = ItemFragment.cartTotalValue - ItemFragment.productValue;

                                                        Log.i("Item removed", "REMOVE CART ITEM");

                                                        Toast.makeText(getActivity(), "Item Removed", Toast.LENGTH_SHORT).show();

                                                        Log.i("Cart Items: ", String.valueOf(ItemFragment.cartItems));

                                                        if(ItemFragment.cartItemNumber == -1)
                                                        {
                                                            ItemFragment.cartEmpty = true;

                                                        }
                                                        //create new shopping cart fragment which has the updated data
                                                        FragmentManager fragmentManager = getFragmentManager();
                                                        Fragment fragment = new ShoppingCart();

                                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.homePageLayout, fragment).addToBackStack(null);
                                                        fragmentTransaction.commit();

                                                    }

                                                }
                                            });
                                        }


                                    }
                                }
                            });
                            builder.show();

                        }
                    });

                    }

                    @NonNull
                    @Override
                    public CartViewHolderAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_list_layout, parent, false);
                        CartViewHolderAdapter holder = new CartViewHolderAdapter(view);
                        return holder;

                    }
                };

            recyclerView.setAdapter(adapter);
            adapter.startListening();
        }

    }


}
