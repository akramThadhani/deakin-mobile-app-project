package com.example.myproject;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ConfirmationListAdapter extends RecyclerView.ViewHolder {


    public TextView productTitle, productPrice;



    public ConfirmationListAdapter(@NonNull View itemView) {
        super(itemView);

        productTitle = itemView.findViewById(R.id.titleTextView);
        productPrice = itemView.findViewById(R.id.priceTextView);
    }
}
