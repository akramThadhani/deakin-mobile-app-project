package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class ProductPage extends AppCompatActivity implements com.example.myproject.ProductAdapter.onItemListener {


    //initialize recycler view variable
    RecyclerView productRecyclerView;
    //create a new list of item type to be used in the product Adapter
    public List<Item> itemList = new ArrayList<>();
    ProductAdapter productAdapter;

    public static boolean isPerfume = false;
    public static boolean isFurniture = false;

    public static int itemPos;

    public static List<Item> cartList;

    public static Integer[] perfumeImageList;
    public static String[] perfumeTitleList;
    public static  String[] perfumePriceList;

    public static Integer[] furnitureImageList;
    public static String[] furnitureTitleList;
    public static  String[] furniturePriceList;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);

        // Get the Intent that started this activity and extract the string
        intent = getIntent();


        //Load array data on to recycler view
        loadArrayData();

        productRecyclerView = findViewById(R.id.productRecyclerView);

        productAdapter = new ProductAdapter(itemList, ProductPage.this, this);
        //we use the adapter and set it with the recycler view element
        productRecyclerView.setAdapter(productAdapter);
        //to keep the orientation fixed in a vertical format we implement a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //we set this layout manager to our Recycler view
        productRecyclerView.setLayoutManager(layoutManager);

        cartList = itemList;
        int listSize = itemList.size();


    }

    public void loadArrayData()
    {
        if (intent.hasExtra("Perfumes"))
        {   isFurniture = false;
            isPerfume = true;

            Intent perfumeIntent = intent;
            // Capture the layout's TextView and set the string as its text
            TextView textView = findViewById(R.id.headerTextView);
            textView.setText("Perfume Bottles");

            //initializing the arrays to store the data to be used on the recycler view 1
            //Glass perfume bottles, furniture, Kitchen is licensed under CC0 1.0
            perfumeImageList = new Integer[]{R.drawable.perfumebottleone, R.drawable.perfumebottletwo, R.drawable.perfumebottlethree,};
            perfumeTitleList = new String[] {"Perfume bottle 1", "Perfume bottle 2", "Perfume bottle 3"};
            perfumePriceList = new String[] {"$30", "$40", "$35" };

            //for loop to load all data in the List array
            for (int i = 0; i < perfumeImageList.length; i++) {
                int image = perfumeImageList[i];
                String title = perfumeTitleList[i];
                String price = perfumePriceList[i];

                Item item = new Item(i, image, title, price);
                itemList.add(item);

            }

        }
        if (intent.hasExtra("Furniture"))
        {   isPerfume = false;
            isFurniture = true;
            String message = intent.getStringExtra(HomePage.FURNITURE_MESSAGE);
            // Capture the layout's TextView and set the string as its text
            TextView textView = findViewById(R.id.headerTextView);
            textView.setText(message);

            //initializing the arrays to store the data to be used on the recycler view 1
            //Glass perfume bottles, furniture, Kitchen is licensed under CC0 1.0
            furnitureImageList = new Integer[]{R.drawable.furniture, R.drawable.furnituretwo};
            furnitureTitleList = new String[]{"Furniture1", "Furniture2"};
            furniturePriceList = new String[]{"$30", "$40"};

            //for loop to load all data in the List array
            for (int i = 0; i < furnitureImageList.length; i++) {
                int image = furnitureImageList[i];
                String title = furnitureTitleList[i];
                String price = furniturePriceList[i];

                Item item = new Item(i, image, title, price);
                itemList.add(item);

            }
        }
    }
    public void newItemFragment()
    {   overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new ItemFragment();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.prodPageLayout, fragment).addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void newShoppingCartActivity()
    {
       //new shopping cart fragment

    }

    @Override
    public void onItemClick(int position) {

        Log.i("Info: Item clicked ", "onItemClick: " + position);
        if (isPerfume)
        {   itemPos = productAdapter.pos ;
            //if clicked position is 0 then open up Perfume 1 details
            if (productAdapter.pos == 0)
            {
                Log.i("Position : " + position, "onItemClick: Perfume1");
                newItemFragment();

            }
            //if clicked position is 0 then open up ABC news fragment etc
            if (productAdapter.pos == 1)
            {
                Log.i("Position : " + position, "onItemClick: Perfume2");
                newItemFragment();
            }
            if (productAdapter.pos == 2)
            {

                Log.i("Position : " + position, "onItemClick: Perfume3");
                newItemFragment();


            }

        }
        if (isFurniture)
        { itemPos = productAdapter.pos;
            //if clicked position is 0 then open up Furniture 1 details
            if (productAdapter.pos == 0)
            {

                Log.i("Position : " + position, "onItemClick: Furniture1");
                    newItemFragment();
            }
            //if clicked position is 1 then open up Furniture 2 details
            if (productAdapter.pos == 1)
            {

                Log.i("Position : " + position, "onItemClick: Furniture2");
                newItemFragment();
            }

        }


    }
    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            //additional code
            Log.i("onBackpressed: ", "going back");

        } else
        {
            getSupportFragmentManager().popBackStack();
        }

    }


}
