package com.example.myproject;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class HomePage extends AppCompatActivity implements com.example.myproject.HomeAdapter.onItemListener {


    public static final String PERFUME_MESSAGE = "Perfumes";
    public static final String FURNITURE_MESSAGE = "Furniture";

    public boolean perfumeClicked = false;
    public boolean furnitureClicked = false;

    public boolean onCartNavItem = false;
    public boolean onHomeNavItem = false;
    public boolean onNotificationNavItem = false;
    public boolean onProfileNavItem = false;

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed;

    public String uid;

    public String snap;
    public static boolean profilePresent = false;


    //initializing the arrays to store the data to be used on the recycler view 1
    //Glass perfume bottles, furniture, Kitchen is licensed under CC0 1.0
    public Integer[] imageList = {R.drawable.perfumebottle, R.drawable.furniture, R.drawable.kitchenutensils,};
    public String[] titleList = {"Perfumes", "Furniture", "Kitchen"};


    //initialize recycler view variable
    RecyclerView productRecyclerView;
    //create a new list of newsOrg type to be used in the news Adapter
    List<Product> productList = new ArrayList<>();
    HomeAdapter homeAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();


        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.userNameText);
        textView.setText("Welcome to Anything Online " );

        //bottom navigation function
        bottomNavigation();

        productRecyclerView = findViewById(R.id.homeRecyclerView);
        homeAdapter = new HomeAdapter(productList, HomePage.this, this);
        //we use the adapter and set it with the recycler view element
        productRecyclerView.setAdapter(homeAdapter);
        //to keep the orientation fixed in a vertical format we implement a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //we set this layout manager to our Recycler view
        productRecyclerView.setLayoutManager(layoutManager);

        //for loop to load all data in the List array
        for (int i = 0; i < imageList.length; i++) {
            int image = imageList[i];
            String title = titleList[i];

            Product product = new Product(i, image, title);
            productList.add(product);

        }

                //check for current user profile details
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        uid = user.getUid();
        Log.i("firebaseUID", uid);
        //set the path to extract data from the DB
        DatabaseReference userListRef1 = FirebaseDatabase.getInstance().getReference().child("User List");
        Log.i("userRef1", String.valueOf(userListRef1));
        //we check if the UID matches with the UID saved in the DB - then we can confirm its the same user

        DatabaseReference hello = userListRef1.child(uid);
        Log.i("hello", hello.toString());

        // This type of listener is not one time, and you need to cancel it to stop
        // receiving updates.
        hello.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try
                {
                    snap = dataSnapshot.child("UID").getValue().toString();
                    Log.i("snap:", snap);


                }
                catch (NullPointerException e)
                {
                    Log.i("snap is null", "ok");
                }

                if (snap!= null)
                {
                    profilePresent = true;
                    Log.i("snap is not null", "profile present true");
                }
                else
                {
                    profilePresent = false;
                    Log.i("snap is null", "profile present false");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }


        });
    }

    public void bottomNavigation()
    {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.nav_home);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.nav_Cart )
                {   onCartNavItem = true;
                    Log.i("BottomNav : ", " Cart selected");
                    cartFragment();

                    return true;
                }
                if (menuItem.getItemId() == R.id.nav_home)
                {   onHomeNavItem = true;
                    Log.i("BottomNav : ", " Home selected");
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                    return true;
                }

                if (menuItem.getItemId() == R.id.nav_profile )
                {
                    onProfileNavItem = true;
                    profileFragment();

                    Log.i("BottomNav : ", " Profile selected");
                    return true;
                }
                return false;
            }
        });

        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.nav_Cart )
                {
                    Log.i("BottomNav : ", " Cart RE selected");

                }
                if (menuItem.getItemId() == R.id.nav_home )
                {
                    Log.i("BottomNav : ", " Home REselected");

                }

                if (menuItem.getItemId() == R.id.nav_profile )
                {
                    Log.i("BottomNav : ", " Profile REselected");

                }

            }
        });
    }



     public void cartFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new ShoppingCart();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.homePageLayout, fragment).addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void profileFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new ProfileFragment();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.homePageLayout, fragment).addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        //end activity and take off the backstack - should not go back to Login page. should log out
        //check with user if they want to log out or continue if back button pressed again then log out.

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 1) {
            super.onBackPressed();
            //additional code
            Log.i("onBackpressed: ", "Now what");

        }
        else
        {
            Log.i("right here", "count ");
            getSupportFragmentManager().popBackStack();
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
            {
//                super.onBackPressed();
                    finish();
                    onStop();
                    onDestroy();
            }
            else { Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show(); }

        }

        mBackPressed = System.currentTimeMillis();


        if (onHomeNavItem)
        {
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
//           onHomeNavItem = false;


        }



        if (onCartNavItem)
        {
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
            onCartNavItem = false;

        }
        if (onNotificationNavItem)
        {
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
            onNotificationNavItem = false;

        }
        if (onProfileNavItem)
        {
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
            onProfileNavItem = false;
        }


        Log.i("onBackFunction: ", "BACK TO Login PAGE");
    }


    @Override
    public void onItemClick(int position) {

        Log.i("Info: ", "onItemClick: " + position);

        if (homeAdapter.pos == 0)
        {
            furnitureClicked = false;
            Log.i("Position : " + position, "onItemClick: Perfumes");
//            newFragment(new nineNews(), R.id.thisLayout);
            perfumeClicked = true;
            newProductPageActivity();
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }

        if (homeAdapter.pos == 1)
        {   perfumeClicked = false;

            Log.i("Position : " + position, "onItemClick: Furniture");
            furnitureClicked = true;
            newProductPageActivity();
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }


    }

    public void newProductPageActivity()
    {
        if (perfumeClicked)
        {
            Intent intent = new Intent(this, ProductPage.class);
            String message = PERFUME_MESSAGE;
            intent.putExtra(PERFUME_MESSAGE, message);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
        else if (furnitureClicked)
        {
            Intent intent = new Intent(this, ProductPage.class);
            String message = FURNITURE_MESSAGE;
            intent.putExtra(FURNITURE_MESSAGE, message);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }



    }
}
