package com.example.myproject;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemFragment extends Fragment {


    private boolean perfumeOptions = ProductPage.isPerfume;
    private boolean furnitureOptions = ProductPage.isFurniture;
    private int itemPosition = ProductPage.itemPos;

    ImageView itemImageView;
    TextView itemTitle;
    TextView itemPrice;
    TextView itemDesc;

    public List<Item> itemList;

    public static int cartItems = 0;
    public static boolean cartEmpty = true;

    public static int cartItemNumber = -1;

    public ImageView cartImageView;

    public static int cartTotalValue = 0;

    public static int productValue = 0;

    public ItemFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_item, container, false);

        itemList = ProductPage.cartList;

        Button addToCartButton = (Button) view.findViewById(R.id.addToCart);
        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cartItemNumber++;
                Log.i("Cart item number", String.valueOf(cartItemNumber));
                cartItems++;

                Log.i("Number of cart Items: ", String.valueOf(cartItems));
                if (cartItems > 0)
                {
                    cartEmpty = false;
                }
                else
                {
                    cartEmpty = true;
                }


                Log.i("Add to cart Clicked","Add to cart Func");
                addToCartFunction();

            }
        });

        loadFragmentDetails(view, itemPosition);

        return view;

    }

    private void loadFragmentDetails(View view, int itemPosition)
    {
        if(perfumeOptions)
        {
            if (itemPosition == itemPosition)
            {
                Log.i("Item Fragment: ", "Display Perfume bottle details");
                itemImageView = (ImageView) view.findViewById(R.id.mainItemImage);
                itemTitle = (TextView) view.findViewById(R.id.itemTextTitle);
                itemPrice = (TextView) view.findViewById(R.id.itemTextPrice);
                itemDesc = (TextView) view.findViewById(R.id.itemTextDescription);


                itemImageView.setImageResource(ProductPage.perfumeImageList[itemPosition]);
                itemTitle.setText(ProductPage.perfumeTitleList[itemPosition]);
                itemPrice.setText(ProductPage.perfumePriceList[itemPosition]);
                itemDesc.setText("This item is currently Available, click on Buy to add this item to your cart");


                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getTitle()));
                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getPrice()));
                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getImage()));


            }

        }
        if (furnitureOptions)
        {
            if (itemPosition == itemPosition)
            {
                Log.i("Item Fragment: ", "Display furniture details");
                itemImageView = (ImageView) view.findViewById(R.id.mainItemImage);
                itemTitle = (TextView) view.findViewById(R.id.itemTextTitle);
                itemPrice = (TextView) view.findViewById(R.id.itemTextPrice);
                itemDesc = (TextView) view.findViewById(R.id.itemTextDescription);

                itemImageView.setImageResource(ProductPage.furnitureImageList[itemPosition]);
                itemTitle.setText(ProductPage.furnitureTitleList[itemPosition]);
                itemPrice.setText(ProductPage.furniturePriceList[itemPosition]);
                itemDesc.setText("This item is currently Available, click on Buy to add this item to your cart");

                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getTitle()));
                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getPrice()));
                Log.i("Member name: ", String.valueOf(itemList.get(itemPosition).getImage()));


            }
        }

    }

    public void addToCartFunction()
    {
        Log.i("add to cart clicked", " Add item to cart");

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String userId= user.getUid();

            if (user!=null)
            {
                String saveTime, saveDate;
                Calendar callForDate = Calendar.getInstance();
                SimpleDateFormat currentDate = new SimpleDateFormat("MM dd, yyyy");
                saveDate = currentDate.format(callForDate.getTime());

                SimpleDateFormat currentTime = new SimpleDateFormat("HH:MM:SS a");
                saveTime = currentTime.format(callForDate.getTime());

                Log.i("Current user", userId);
                Log.i("Current date", saveDate);
                Log.i("Current time", saveTime);

                String itemValue = itemPrice.getText().toString();

                Log.i("itemValue: ", itemValue);

                productValue = Integer.parseInt(itemValue.replaceAll("[\\D]",""));

                Log.i("IValue: ", String.valueOf(productValue));

                cartTotalValue = cartTotalValue + productValue;

                DatabaseReference cartListRef = FirebaseDatabase.getInstance().getReference().child("Cart List");

                final HashMap<String, Object> cartMap = new HashMap<>();
                cartMap.put("title", itemTitle.getText().toString());
                cartMap.put("price", itemPrice.getText().toString());
                cartMap.put("Date", saveDate);
                cartMap.put("Time", saveTime);

                String dbItemRef = itemTitle.getText().toString();

                cartListRef.child("User View").child(userId)
                        .child("Items")
                        .child(dbItemRef)
                        .updateChildren(cartMap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful())
                                {
                                    Log.i("Task: ","Success");


                                }

                            }
                        });

            }

    }



}
