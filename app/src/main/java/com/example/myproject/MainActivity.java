package com.example.myproject;


import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.storage.StorageReference;

public class MainActivity extends AppCompatActivity {

//    public static final String EXTRA_MESSAGE = null;
    public EditText email,password;

    public static String name;
    public static String emailString, passwordString, userNameString;

    private FirebaseAuth mAuth;

    //this is the button function which checks if the user email exists
    //if so then is considered a current user
    public void loginFunction(View view)
    {

        email = (EditText) findViewById(R.id.emailEditText);
        emailString = email.getText().toString();
        password = (EditText) findViewById(R.id.passwordEditText);
        passwordString = password.getText().toString();
//        userName = (EditText) findViewById(R.id.userNameEditText);
//        userNameString = userName.getText().toString();

//        name = userNameString;

        Log.i("Info:", "Login clicked");
        Log.i("Email: ", emailString);
        Log.i("Password: ", passwordString);
//        Log.i("User Name: ", userNameString);

        //check if the user is active (code from firebase.com - authentication guidelines)

        //if the email the user entered is NOT null then proceed..
        if (email.getText().toString() != null )
        {   //method to check if email belongs to existing user
            mAuth.fetchSignInMethodsForEmail(email.getText().toString())
            .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                @Override
                public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                    //return results if true (email has not been found)
                    boolean checkUserEmail = task.getResult().getSignInMethods().isEmpty();
                    if (checkUserEmail)
                    {
                        Log.i("Email: ", "Not found");
                        Toast.makeText(MainActivity.this, "Login failed. New user?", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {   //email has been found and Not empty result = existing user
                        Log.i("Email: ", "Found");
                        //then proceed with existing user function
                        existingUser();

                    }
                }
            });

        }

    }

    //Changing the UI according to the User login details
    public void  updateUI(FirebaseUser account){
        if(account != null)
        {
            Log.i("UpdateUI: ", "Success");
            Toast.makeText(this,"Login Successful",Toast.LENGTH_LONG).show();
            //create new intent to login to home page of application
            newHomePageActivity();

        }

    }
    //existing user function - checks if the user email is in the database
    //if yes then user is existing and login is successful
    public void existingUser()
    {//this function gets the current user from the login details and updates the current user
        mAuth.signInWithEmailAndPassword(emailString, passwordString)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
        {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    // successful match from email, update UI with the signed-in user's information
                    Log.i("Success: ", "Existing user function");
                    FirebaseUser user = mAuth.getCurrentUser();
                    String currentUser = user.getEmail();
                    Log.i("Current User ", String.valueOf(currentUser));
                    updateUI(user);

                }
                else
                {
                    // If sign in fails, display a message to the user.
                    Log.i("Fail:", "Existing user Function");
                    updateUI(null);

                    // ...
                }

                // ...
            }
        });
    }
    //this is the button function which checks if the user already exists or not
    public void signUpFunction(View view)
    {
        mAuth.fetchSignInMethodsForEmail(email.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                        //return results if true (email has not been found)
                        boolean checkUserEmail = task.getResult().getSignInMethods().isEmpty();
                        if (checkUserEmail)
                        {   //then the user does not exist in app so create new user
                            signUp();
                            Log.i("New user: ", "Created");

                        }
                        else
                        {   //email has been found and Not empty result = existing user
                            Log.i("Email: ", "User Exists");
                            //then display message saying user already exists
                            Toast.makeText(MainActivity.this, "User already Exists", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
    //default android function for signing up new users
    public void signUp()
    {
        mAuth.createUserWithEmailAndPassword(emailString, passwordString)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    // Sign in success, update UI with the signed-in user's information
                    Log.i("SignUpFunc", "Success new sign up");

                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                }
                else
                {
                    // If sign in fails, display a message to the user.
                    Log.i("Sign Up", "FAILED ");
                    updateUI(null);
                }

                // ...
            }
        });
    }
    //function to create new activity for user to access home page
    public void newHomePageActivity()
    {
        Intent intent = new Intent(this, HomePage.class);
//        EditText username = (EditText) findViewById(R.id.userNameEditText);
//       String message = username.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();





    }






}


