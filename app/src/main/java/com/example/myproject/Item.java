package com.example.myproject;

public class Item {

    private int id, image;
    private String title;
    private String price;

    public Item()
    {

    }

    //class constructor
    public Item(int id, int image, String title, String price)
    {
        this.id = id;
        this.image = image;
        this.title = title;
        this.price = price;
    }

    //create getter and setter methods for the ID
    public  int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    //create getter and setter methods for image view and text view
    public int getImage()
    {
        return image;
    }
    public void setImage(int image)
    {
        this.image = image;
    }
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getPrice()
    {
        return price;
    }
    public void setPrice(String price)
    {
        this.price = price;
    }
}
