package com.example.myproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class orderArrived extends Fragment {

    public orderArrived() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view;
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_order_arrived, container, false);

        Button okButton = view.findViewById(R.id.okBtn);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("Ok button", "Clicked OrderArrivedFrag");

                onDestroy();
                getActivity().finish();
                //create new home page activity for the user

                Intent intent = new Intent(getContext(), HomePage.class);
                startActivity(intent);


            }
        });


        return view;
    }
}
