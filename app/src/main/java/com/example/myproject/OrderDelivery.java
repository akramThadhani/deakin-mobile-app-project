package com.example.myproject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static androidx.core.content.ContextCompat.getSystemService;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDelivery extends Fragment {

    ProgressBar progressBar;
    TextView arrived;
    private Handler handler = new Handler();

    public boolean hasArrived = false;
    public int mProgressStatus = 0;

    public int i = 0;

    public static final String CHANNEL_1_ID = "channel1";

    public NotificationManagerCompat notificationManagerCompat;

    public OrderDelivery() {
        // Required empty public constructor

    }


    @SuppressLint("StaticFieldLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_order_delivery, container, false);

        //create notification channel for delivery notification
        notificationChannel();

        notificationManagerCompat = NotificationManagerCompat.from(getContext());

        TextView headerText = view.findViewById(R.id.processTextView);
        headerText.setText("Tracking your order.........");

        progressBar = view.findViewById(R.id.progressBar);

        arrived = view.findViewById(R.id.orderArrivedTextView);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        progressBar.setProgress(mProgressStatus);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {
                for (i = 0; i <= 200; i++) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mProgressStatus += 1;

                    handler.post(new Runnable() {
                        public void run() {

                            Log.i("progress: ", String.valueOf(i));

                            if (i == 100)
                            {
                                arrived.setText("Your order will be arriving shortly.....");
                            }

                            if (i == 200)
                            {
                                progressBar.setIndeterminate(false);
                                progressBar.setProgress(i);

                                arrived.setText("Your order has arrived!!");

//                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                                final String userId = user.getUid();

                                final DatabaseReference cartListRef = FirebaseDatabase.getInstance().getReference().child("Cart List");
                                cartListRef.removeValue();

                                ItemFragment.cartItemNumber = 0;
                                ItemFragment.cartItems = 0;
                                ItemFragment.cartTotalValue = 0;

                                ItemFragment.cartEmpty = true;

                                hasArrived = true;


                            }
                            if (hasArrived == true)
                            {
                                notificationOne();
                            }


                        }
                    });
                }
                return null;
            }

        }.execute();

        return view;


    }
    //notification channel creation to use with the Notification Builder
    public void notificationChannel()
    {
        //checking if the version is for API level 26 for the notification to work
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(CHANNEL_1_ID, "Your Order",NotificationManager.IMPORTANCE_HIGH);

            channel.setDescription("Channel for high priority notifications like order delivery arrival");

            NotificationManager manager = getActivity().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);


        }

    }

    public void notificationOne()
    {

        // prepare intent which is triggered if the
        // notification is selected

        Intent intent = new Intent(getContext(), Proceed.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        //create the notification

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), CHANNEL_1_ID)
                .setSmallIcon(R.drawable.anythingonlinelogo)
                .setAutoCancel(true)
                .setContentTitle("Anything Online")
                .setContentText("Your order has arrived!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(contentIntent);

                notificationManagerCompat.notify(1, builder.build());

                Log.i("Order Arrived", "True");

                Proceed.orderArrived = true;



    }



}


